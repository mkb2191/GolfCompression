def byterange(string):
    string = list(map(ord, string))
    minbyte = min(string)
    maxbyte = max(string)
    string = [i - minbyte for i in string]
    bytesize = len(bin(maxbyte - minbyte)[2:])
    compressed = [minbyte, bytesize]
    number = int((bytesize * len(string) % 8) / bytesize)
    bits = 4
    for i in string:
        number = number << bytesize
        number += i
        bits += bytesize
        while bits >= 16:
            amount = bits - 16
            byte = number >> amount
            number -= byte << amount
            compressed.append(byte)
            bits -= 16
    if bits:
        byte = number << (16 - bits)
        compressed.append(byte)
    return ''.join(map(chr, compressed))

def byterangedecompress(compressed):
    minbyte = ord(compressed[0])
    bytesize = ord(compressed[1])
    compressed = list(map(ord, compressed[2:]))
    string = []
    spare = compressed[0] >> 4
    number = compressed[0] - (spare << 4)
    bits = 4
    for i in compressed[1:]:
        number = number << 16
        number += i
        bits += 16
        while bits >= bytesize:
            amount = bits - bytesize
            byte = number >> amount
            number -= byte << amount
            string.append(byte)
            bits -= bytesize
    if spare:
        string = string[:-spare]
    string = [i + minbyte for i in string]
    return ''.join(map(chr, string))
