#! /usr/bin/env python3
import argparse
import zlib

def compress(args):
    with open(args.file, 'rb') as file:
        data = file.read()
    compressed = zlib.compress(data, 9)
    with open('a.golf', 'wb') as file:
        file.write(compressed)

def decompress(args):
    with open(args.file, 'rb') as file:
        data = file.read()
    decompressed = zlib.decompress(data)
    print(decompressed.decode('utf-8'))

def main():
    '''Parse commandline arguments and run program'''
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='Input file')
    parser.add_argument('-c', '--compress', action="store_true",
                        help='Compress file')
    args = parser.parse_args()
    if args.compress:
        compress(args)
    else:
        decompress(args)

if __name__ == '__main__':
    main()
